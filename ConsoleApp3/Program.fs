﻿open System
open System.Runtime.InteropServices

module Program =

  [<DllImport("Dll1.dll")>]
  extern IntPtr Mensaje()

  let [<EntryPoint>] main _ =
    Console.Title <- "Consola F# 2019";
    Console.WriteLine(Marshal.PtrToStringAuto(Mensaje()))
    Console.ReadKey() |> ignore
    0