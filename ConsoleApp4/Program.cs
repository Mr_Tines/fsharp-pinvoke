﻿using System;
using System.Runtime.InteropServices;

namespace Consola_cs
{
  internal class Program
  {
    private static void Main(string[] args)
    {
      Console.WriteLine(Marshal.PtrToStringAuto(SUPER_DLL.Mensaje()));

      // Pulse cualquier tecla para salir.
      Console.ReadKey();
    }

    internal class SUPER_DLL
    {
      [DllImport("Dll1.dll")]
      extern static public IntPtr Mensaje();
    }
  }
}